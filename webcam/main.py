# ..
import time
from collections import deque
from threading import Thread

# ...
from config import CustomConfig
from webcam import WebCam
from api import get_license_plates

# finish
finish = False

# Queueing
img_queue = deque()

conf = CustomConfig()


# ...........

def finish_all():
    input("Press Enter to continue...")
    global finish
    finish = True


def api_requester():
    def worker_thread(image):
        plates = get_license_plates(conf.get_api_url(), conf.is_debug_mode_on(), image)
        if plates is None:
            return
        plate = plates[0]
        if float(plate["confidence"]) > conf.get_success_treshold():
            print(plate["license_plate"], " with ", plate["confidence"], " probability")
            img_queue.clear()

    while not finish:
        if not img_queue:
            continue
        img = img_queue.popleft()
        Thread(target=worker_thread(img)).start()


def frame_uploader():
    web_cam = WebCam(conf.get_prop_id())
    start = time.time()
    while not finish:
        img = web_cam.get_image(conf.show_frame())
        end = time.time()
        if (end - start) <= conf.get_img_delay():
            continue
        start = end
        img_queue.append(img)

    web_cam.close_cam()


worker_threads = [
    Thread(target=frame_uploader),
    Thread(target=api_requester),
    Thread(target=api_requester),
    Thread(target=api_requester),
    Thread(target=api_requester),
    Thread(target=finish_all)]


def start_threads():
    for t in worker_threads:
        t.start()

    for t in worker_threads:
        t.join()


start_threads()
