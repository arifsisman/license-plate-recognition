import cv2
from PIL import Image
from six import BytesIO


class WebCam:

    def __init__(self, prop_id):
        self.cap = cv2.VideoCapture(prop_id)

    def get_image(self, show_frame):
        # Capture frame-by-frame
        ret, frame = self.cap.read()

        frame_im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        pil_im = Image.fromarray(frame_im)
        stream = BytesIO()
        pil_im.save(stream, format="JPEG")
        stream.seek(0)

        if show_frame:
            cv2.imshow("DD", frame)

        cv2.waitKey(1)
        return stream.read()

    def close_cam(self):
        # When everything done, release the capture
        self.cap.release()
