import requests


def get_license_plates(url, debug, img):
    response = requests.post(url, files={'uploadFile': img})

    if debug:
        print(response.text)

    try:
        json = response.json()
    except ValueError:  # includes simplejson.decoder.JSONDecodeError
        print('Decoding JSON has failed')
        return

    if json["err"] == 1:
        if debug:
            print("Err", json["response"])
        return None
    result = json["result"]
    if result["count"] <= 0:
        if debug:
            print("Not any license-plate found", json["response"])
        return None

    return result["predictions"]
