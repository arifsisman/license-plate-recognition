import configparser


# TODO: use config.ini file


def __str_to_bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise ValueError  # evil ValueError that doesn't tell you what the wrong value was


class CustomConfig:

    def __init__(self):
        self.__parser = configparser.ConfigParser()
        self.__read_config()
        self.__properties = {
            "PropId": {
                "Section": "WebCam",
                "value": None
            },
            "Img_Delay": {
                "Section": "Parameters",
                "value": None
            },
            "Success_Treshold": {
                "Section": "Parameters",
                "value": None
            },
            "URL": {
                "Section": "API",
                "value": None
            },
            "Debug": {
                "Section": "Others",
                "value": None
            },
            "Show_Frame": {
                "Section": "Others",
                "value": None
            },
        }

    def __read_config(self):
        self.__parser.read("config.ini")

    @staticmethod
    def __convert(value, converter=None):
        if converter is None:
            return value
        return converter(value)

    def __get_property(self, param, getter):
        # TODO : consider refreshing settings
        # self.__reset_config()
        section = self.__properties[param]["Section"]
        old_value = self.__properties[param]["value"]
        new_value = getter(section, param)
        if (old_value is not None) and (not old_value == new_value):
            print(param, "property changed to :", old_value, "=>", new_value)
        self.__properties[param]["value"] = new_value
        return self.__properties[param]["value"]

    def get_prop_id(self):
        return self.__get_property("PropId", self.__parser.getint)

    def get_img_delay(self):
        return self.__get_property("Img_Delay", self.__parser.getfloat)

    def get_success_treshold(self):
        return self.__get_property("Success_Treshold", self.__parser.getfloat)

    def get_api_url(self):
        return self.__get_property("URL", self.__parser.get)

    def is_debug_mode_on(self):
        return self.__get_property("Debug", self.__parser.getboolean)

    def show_frame(self):
        return self.__get_property("Show_Frame", self.__parser.getboolean)
